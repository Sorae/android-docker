# Android-docker

A custom docker image made for personal Android development and CI.

Based on https://hub.docker.com/r/androidsdk/android-31/dockerfile and topped with an extra python3 installation.