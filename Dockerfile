FROM androidsdk/android-31:latest
USER root

# Install Python3 
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 --no-cache-dir install --upgrade pip \
  && rm -rf /var/lib/apt/lists/*

# Pip install dependencies
RUN pip3 install google-api-python-client oauth2client